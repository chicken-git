#!/bin/sh
#|
exec "$CHICKEN_CSI" -s "$0" "$@"
|#

(include "custom-build.scm")

(compile ,@(command-line-arguments) -D ,libgit2-X.X -D ,libgit2-X.X.X -L -lgit2)

;; If we couldn't detect libgit2's version via pkg-config, check it directly now.
(when (not pkg-config-version)
  (load "./libgit2.so")
  (load "./libgit2.import.so")
  (import (only (git libgit2) libgit2-version))
  ;; `libgit2-version` returns a vector `#(<major> <minor> <patch>)`
  (unless (libgit2-version-ok? (apply format "~a.~a.~a" (vector->list (libgit2-version))))
    (error "git: This extension requires libgit2" *required-libgit2*)))
