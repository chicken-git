(export
 blob*-create-frombuffer
 blob*-create-fromdisk
 blob*-create-fromworkdir
 blob*-free
 blob*-id
 blob*-is-binary
 blob*-lookup
 blob*-rawcontent
 blob*-rawsize
 blob-create-frombuffer
 blob-create-fromdisk
 blob-create-fromworkdir
 blob-free
 blob-id
 blob-is-binary
 blob-lookup
 blob-rawcontent
 blob-rawsize
 branch-create
 branch-delete
 branch-is-head
 branch-iterator-new
 branch-iterator-free
 branch-lookup
 branch-move
 branch-name
 branch-next
 checkout-head
 checkout-index
 checkout-tree
 checkout-options-default
 clone
 commit-author
 commit-committer
 commit-create
 commit-free
 commit-id
 commit-lookup
 commit-message
 commit-message-encoding
 commit-message-raw
 commit-nth-gen-ancestor
 commit-parent
 commit-parent-id
 commit-parentcount
 commit-time
 commit-time-offset
 commit-tree
 commit-tree-id
 commit-raw-header
 config-delete-entry
 config-entry-level
 config-entry-name
 config-entry-value
 config-find-global
 config-find-system
 config-find-xdg
 config-free
 config-get-bool
 config-get-int64
 config-get-string
 config-get-string-buf
 config-open-default
 config-open-ondisk
 config-set-bool
 config-set-int64
 config-set-string
 diff-delta-new-file
 diff-delta-old-file
 diff-delta-similarity
 diff-delta-status
 diff-file-flags
 diff-file-mode
 diff-file-id
 diff-file-path
 diff-file-size
 diff-find-similar
 diff-foreach
 diff-free
 diff-num-deltas
 diff-num-deltas-of-type
 diff-hunk-header
 diff-hunk-header-length
 diff-hunk-new-lines
 diff-hunk-new-start
 diff-hunk-old-lines
 diff-hunk-old-start
 diff-index-to-workdir
 diff-line-content
 diff-line-content-length
 diff-line-content-offset
 diff-line-new-lineno
 diff-line-num-lines
 diff-line-old-lineno
 diff-line-origin
 diff-tree-to-index
 diff-tree-to-tree
 diff-tree-to-workdir
 filemode->int ; Used by `tree-entry-attributes`.
 git-error
 ignore-add-rule
 ignore-clear-internal-rules
 ignore-path-is-ignored
 index-add
 index-add-bypath
 index-clear
 index-entry-dev
 index-entry-extended
 index-entry-flags
 index-entry-gid
 index-entry-ino
 index-entry-mode
 index-entry-mtime
 index-entry-id
 index-entry-path
 index-entry-size
 index-entry-stage
 index-entry-uid
 index-entrycount
 index-find
 index-free
 index-get-byindex
 index-get-bypath
 index-open
 index-read
 index-remove
 index-time-seconds
 index-write
 index-write-tree
 libgit2-init
 libgit2-shutdown
 libgit2-version
 make-git-condition
 merge-base
 note-create
 note-default-ref
 note-foreach
 note-free
 note-message
 note-id
 note-read
 note-remove
 object-id
 object-lookup
 object-owner
 object-type
 odb-exists
 odb-free
 odb-hash
 odb-object-data
 odb-object-free
 odb-object-id
 odb-object-size
 odb-object-type
 odb-open
 odb-read
 odb-write
 oid-cpy
 oid-equal
 oid-fromstr
 oid-pathfmt
 oid-tostr
 patch-free
 patch-from-blob-and-buffer
 patch-from-blobs
 patch-from-diff
 patch-get-delta
 patch-get-hunk
 patch-get-line-in-hunk
 patch-line-stats
 patch-num-hunks
 patch-num-lines-in-hunk
 patch-size
 patch-to-buf
 reference-create
 reference-delete
 reference-foreach-name
 reference-foreach-glob
 reference-free
 reference-is-branch
 reference-is-remote
 reference-is-tag
 reference-lookup
 reference-name
 reference-name-to-id
 reference-rename
 reference-resolve
 reference-set-target
 reference-symbolic-create
 reference-target
 reference-type
 refspec-direction
 refspec-dst
 refspec-force
 refspec-src
 refspec-string
 remote-add-fetch
 remote-add-push
 remote-connect
 remote-connected
 remote-create
 remote-disconnect
 remote-download
 remote-fetch
 remote-free
 remote-get-refspec
 remote-is-valid-name
 remote-list
 remote-lookup
 remote-name
 remote-pushurl
 remote-rename
 remote-refspec-count
 remote-set-pushurl
 remote-set-url
 remote-stats
 remote-stop
 remote-update-tips
 remote-url
 repository-config
 repository-discover
 repository-free
 repository-head
 repository-head-detached
 repository-head-unborn
 repository-index
 repository-init
 repository-is-bare
 repository-is-empty
 repository-odb
 repository-open
 repository-path
 repository-workdir
 revparse
 revparse-single
 revspec-from
 revspec-to
 revwalk-free
 revwalk-hide
 revwalk-new
 revwalk-next
 revwalk-push
 revwalk-push-head
 revwalk-sorting
 signature-default
 signature-dup
 signature-email
 signature-free
 signature-name
 signature-new
 signature-now
 signature-time
 status-file
 status-foreach
 status-should-ignore
 tag-create
 tag-delete
 tag-foreach
 tag-free
 tag-id
 tag-lookup
 tag-message
 tag-name
 tag-peel
 tag-tagger
 tag-target
 time-offset
 time-time
 transfer-progress-indexed-objects
 transfer-progress-received-bytes
 transfer-progress-received-objects
 transfer-progress-total-objects
 tree-builder-clear
 tree-builder-free
 tree-builder-get
 tree-builder-insert
 tree-builder-new
 tree-builder-remove
 tree-builder-write
 tree-entry-byindex
 tree-entry-byname
 tree-entry-byoid
 tree-entry-bypath
 tree-entry-dup
 tree-entry-filemode
 tree-entry-free
 tree-entry-id
 tree-entry-name
 tree-entry-to-object
 tree-entry-type
 tree-entrycount
 tree-free
 tree-id
 tree-lookup
 tree-walk)
