;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; custom-build.scm - git egg installation script
;;;
;;; Copyright (c) 2013-2021, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(import (chicken io)
        (chicken irregex)
        (chicken platform)
        (chicken process)
        (chicken process-context)
        (chicken string))

;;
;; Shims for CHICKEN 4 setup API
;;

(define (version>=? v1 v2)
  (let loop ((v1 (string-split v1 "."))
             (v2 (string-split v2 ".")))
    (or (null? v2)
        (and (pair? v1)
             (let ((n1 (string->number (car v1)))
                   (n2 (string->number (car v2))))
               (or (> n1 n2)
                   (and (= n1 n2)
                        (loop (cdr v1) (cdr v2)))))))))

(define-syntax compile
  (er-macro-transformer
   (lambda (x r c)
     `(let ((n (nth-value 2 (process-wait
                             (process-run
                              (get-environment-variable "CHICKEN_CSC")
                              (map ->string (eval ',(list 'quasiquote (cdr x)))))))))
        (unless (zero? n) (exit n))))))

;;
;; Declare supported versions.
;;
(define *required-chicken* "5.0")
(define *required-libgit2* "0.23.0")

;;
;; Verify the CHICKEN version.
;;

(unless (version>=? (chicken-version) *required-chicken*)
  (error "git: This extension requires CHICKEN version" *required-chicken*))

;;
;; Verify the libgit2 version.
;;
;; Due to API instability, the library's major and minor versions must
;; match the required version precisely, while its patch version may be
;; equal to or greater than required.
;;

(define (libgit2-version-ok? version-string)
  (version>=? version-string *required-libgit2*))

;; If pkg-config is available, consult it first.
(define pkg-config-version
  (let ((s (with-input-from-pipe "pkg-config --modversion libgit2" read-string)))
    (and (string? s) (string-chomp s))))

;; Assign a libgit2 version feature identifier.
(define libgit2-X.X.X
  (cond
    ((not pkg-config-version)
     (warning "git: Unable to detect libgit2 version, guessing" *required-libgit2*)
     (newline (current-error-port))
     (string-append "libgit2-" *required-libgit2*))
    ((libgit2-version-ok? pkg-config-version)
     (string-append "libgit2-" pkg-config-version))
    (else
     (error "git: This extension requires libgit2 version" *required-libgit2*))))

;; And a short version.
(define libgit2-X.X
  (irregex-replace '(: "." (+ num) eol) libgit2-X.X.X ""))
