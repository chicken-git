# chicken-git

libgit2 bindings for CHICKEN Scheme.

## Install

Obviously, libgit2 is required: <http://github.com/libgit2/>.

Assuming you have that, installation should be straightforward:

    $ git clone https://git.foldling.org/chicken-git.git
    $ cd chicken-git
    $ chicken-install -test

This extension requires libgit2 >= 0.23.0 and CHICKEN >= 5.0.0.

## API

The library is split into two modules, `libgit2` and `git`:

* `libgit2` is essentially just the libgit2 API, thinly wrapped. Most of
  the function signatures remain the same, with a few exceptions:

  * Opaque structures are represented as pointer objects (e.g. `git_commit`).
  * Public structures are represented as locatives (e.g. `git_oid`).
  * Memory for out parameters and other structures that would typically
    go on the stack is allocated automatically.
  * Return values are checked where appropriate, signaling an exception of type
    `(exn git)` when non-zero.

* `git` is a higher-level interface around `libgit2`, providing record types
  for each libgit2 structure and a more Scheme-friendly API.

Documentation is available at <http://wiki.call-cc.org/egg/git>.

## Notes

Some functionality is not yet provided, such as custom backends, remotes and
reflog inspection. Of course, patches are more than welcome.

## Contact

  * Evan Hanson <evhan@foldling.org>

## License

BSD. See LICENSE for details.
